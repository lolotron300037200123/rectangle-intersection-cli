import argparse


def normalize_rectangle(x1, y1, x2, y2):
    """Rearranges rectangle point coordinates to be sane

    Sane means rectangle is defined by it's left-bottom and
    right-top corners. Should't raise anything, if given
    int or float arguments, as all coordinates should be

    Args:
        x1, y1:   First point coordinates,
        x2, y2:   Second point coordinates

    Returns:
        4 coordinates of two points of a rectangle in this order:
        x-left, y-bottom, x-right, y-top

    Examples:
        >>> normalize_rectangle(1, 1, 3, 3)
        (1, 1, 3, 3)

        >>> normalize_rectangle(3, 3, 1, 1)
        (1, 1, 3, 3)

        >>> normalize_rectangle(1, 3, 3, 1)
        (1, 1, 3, 3)
    """
    return (
        min(x1, x2),
        min(y1, y2),
        max(x1, x2),
        max(y1, y2)
    )


def intersection(x1, y1, x2, y2, x3, y3, x4, y4) -> int | float:
    """Find intersection area of two rectangles

    Rectangle coordinates are normalized internally, so you
    don't have to do it yourself

    Args:
        x1, y1, x2, y2: First rectangle corners coordinates
        x3, y3, x4, y4: Second rectangle corners coordinates

    Returns:
        Intersection area. If there is no intersection returns 0

    Raises:
        TypeError
    """
    x1, y1, x2, y2 = normalize_rectangle(x1, y1, x2, y2)
    x3, y3, x4, y4 = normalize_rectangle(x3, y3, x4, y4)
    dx = max(0, min(x2, x4) - max(x1, x3))
    dy = max(0, min(y2, y4) - max(y1, y3))
    return dx * dy


def union(x1, y1, x2, y2, x3, y3, x4, y4) -> int | float:
    """Find union area of two rectangles

    Calculated by subtracting intersection are of the
    rectangles from sum of their areas. Coordinates are
    normalized internally, so you don't have to do it
    yourself


    Args:
        x1, y1, x2, y2: First rectangle corners coordinates
        x3, y3, x4, y4: Second rectangle corners coordinates

    Returns:
        Union area. If the rectangles don't intersect it will just
        be sum of their areas

    Raises:
        TypeError
    """
    x1, y1, x2, y2 = normalize_rectangle(x1, y1, x2, y2)
    x3, y3, x4, y4 = normalize_rectangle(x3, y3, x4, y4)
    area1 = abs(x1 - x2) * abs(y1 - y2)
    area2 = abs(x3 - x4) * abs(y3 - y4)
    intersection_area = intersection(x1, y1, x2, y2, x3, y3, x4, y4)
    return area1 + area2 - intersection_area


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-r1", "--rectangle1", type=float,
                        nargs=4, required=True)
    parser.add_argument("-r2", "--rectangle2", type=float,
                        nargs=4, required=True)
    args = parser.parse_args()

    rect1 = args.rectangle1
    rect2 = args.rectangle2
    intersection_area = intersection(*(*rect1, *rect2))
    print("Intersection area:", intersection_area)
    print("Union area:", union(*(*rect1, *rect2)))


if __name__ == "__main__":
    main()
