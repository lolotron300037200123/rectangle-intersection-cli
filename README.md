# Rectangle intersection

Toolbox for calculating intersection and union area

Do you want to:

-   Calculate intersection area?
-   Calculate union are?
-   ~~Bring back soviet union?~~

We got you covered!

Features:

-   [x] Find intersection area
-   [x] Find union area
-   [ ] Cure cancer _(PRs are welcome)_
-   [ ] Bring 2d into this world _(PRs are **welcome**)_

## How it works

![alt two overlapping rectangles](https://i.stack.imgur.com/kZv00.png)

Formula for calculating intersection area, assuming second rectangle is to the right and higher then the first one, just like on image above:
$$(x2 - x3) \cdot (y4 - y2)$$
Stolen from [this stackoverflow answer](https://stackoverflow.com/questions/27152904/calculate-overlapped-area-between-two-rectangles)

The real code actually finds out how the rectangles are positioned and corrects the formula accordingly (not really, but sure)
And union are is just $area1 + area2 - intersection$. As simple as that

## Usage

You can import it yourself and use to your hearts content. The functions inside are reasonably well documented. Just go ahead and

```python
intersection = intersection_area(1, 1, 3, 3, 2, 2, 4, 4)
union = union_area(1, 1, 3, 3, 2, 2, 4, 4)
print(intersection, union) # Should print 1, 7
```
